#include "pch.h"
#include <utility>
#include <limits.h>
#include <vector>
#include "VisionMatching.h"

using namespace std;

cv::Point VisionMatching:: TemplateMatching(int pointX, int pointY, cv::Mat img1, cv::Mat img2, double *minimumVal)
{

	//Image Size : 2964x2000

	cv::Rect myROI(pointX-11, pointY-11, 21, 21);

	cv::Mat feedTemplate = img1(myROI);

	cv::Mat result;

	double maxVal;

	cv::Point minLoc;
	cv::Point maxLoc;


	imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\template.png",
		feedTemplate);

	matchTemplate(img2, feedTemplate, result, cv::TM_SQDIFF);

	cv::minMaxLoc(result, minimumVal, &maxVal, &minLoc, &maxLoc);

	cv::Rect outROI(minLoc.x, minLoc.y, 21, 21);

	cv::Mat matchedSegment = img2(outROI);
	

	imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\out.png", 
		matchedSegment);


	return minLoc;
}



void VisionMatching::PointMatching(int img1X, int img1Y, int img2X, int img2Y, 
	cv::Mat img1, cv::Mat img2, double* minimumVal)
{

	//Image Size : 2964x2000

	cv::Rect myROI(img1X - 10, img1Y - 10, 21, 21);

	cv::Mat feedTemplate = img1(myROI);

	cv::Rect img2ROI(img2X - 10, img2Y - 10, 21, 21);

	cv::Mat feedImage = img2(img2ROI);

	cv::Mat result;

	double maxVal;

	cv::Point minLoc;
	cv::Point maxLoc;


	imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\template.png",
		feedTemplate);

	matchTemplate(feedImage, feedTemplate, result, cv::TM_SQDIFF);

	cv::minMaxLoc(result, minimumVal, &maxVal, &minLoc, &maxLoc);

	cv::Rect outROI(minLoc.x, minLoc.y, 21, 21);

	cv::Mat matchedSegment = img2(outROI);


	imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\out.png",
		matchedSegment);


}