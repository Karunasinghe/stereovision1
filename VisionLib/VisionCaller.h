#ifdef VISIONLIB_EXPORTS
#define VISIONLIB_API __declspec(dllexport)
#else
#define VISIONLIB_API __declspec(dllimport)
#endif

//#define STEREO_DEBUG_MODE

extern "C" VISIONLIB_API int StereoTest(const char* img1, const char* img2, double baseline, double fx);

extern "C" VISIONLIB_API int StereoTestUncalibrated(const char* img1, const char* img2);

extern "C" VISIONLIB_API int StereoTestUncalibratedCV(const char* img1, const char* img2);



