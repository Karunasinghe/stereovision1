#include "pch.h"
#include "CoordinateConverter.h"

using namespace cv;

double CoordinateConverter::ComputeZ(double firstImageX, double secondmageX, double baseline, double fx)
{
	/*double baseline = 193.001;
	double fx = 3979.911;*/

	double diff = 0;
	double z = 0;

	diff = (firstImageX - secondmageX);
	z = (fx * baseline) / diff;

	return z;

}