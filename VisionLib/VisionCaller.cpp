#include "pch.h"
#include "VisionCaller.h"
#include "VisionMatching.h"
#include "CoordinateConverter.h"
#include "UncalibratedStereo.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>//Use in Stereo Vision

#include <fstream>
#include <iostream>
#include <windows.h>
#include <string>

#include <sys/stat.h>
#include <direct.h>
#define GetCurrentDir _getcwd

using namespace cv;
using namespace std;

std::string get_current_dir() {
	char buff[FILENAME_MAX]; //create string buffer to hold path
	GetCurrentDir(buff, FILENAME_MAX);
	string current_working_dir(buff);
	return current_working_dir;
}


VISIONLIB_API int StereoTest(const char* imgLink1, const char* imgLink2, double baseline, double fx)
{
	try
	{
		string filesSavePath = get_current_dir()+"\\Results\\";

		// Structure which would store the metadata
		struct stat sb;
		const char* dirPath = filesSavePath.c_str();

		if (stat(dirPath, &sb) != 0)
		{
			int dirCreateStat = _mkdir(dirPath);

			if (dirCreateStat == -1)
				return -1;
		}

		Mat img1 = imread(imgLink1);
		Mat img2 = imread(imgLink2);

		Mat finalImage(img1.rows, img1.cols, CV_64FC1);
		Mat finalImageColoured(img1.rows, img1.cols, CV_8UC3);

#ifdef STEREO_DEBUG_MODE
		ofstream outfiledecleare;

		outfiledecleare.open(filesSavePath + "ImageMatWithoutassignings.txt");

		outfiledecleare << finalImage;

#endif // STEREO_DEBUG_MODE


		CoordinateConverter coordinateConverter;

		VisionMatching visionMatching;

		Vec3d output;

		double maxVal = 0;

		cv::Mat weightMat(cv::Size(img1.cols, img1.rows), CV_8UC1, Scalar(75));

#ifdef STEREO_DEBUG_MODE
		ofstream weightMatOutFile;

		weightMatOutFile.open(filesSavePath + "WeightMat.txt");

		weightMatOutFile << weightMat;
#endif // DEBUG

		double templateMinVal = DBL_MAX;

		for (int i = 50; i < (img1.rows - 100); i += 21)
		{
			for (int img1Col = 20; img1Col < (img1.cols - 30); img1Col += 21)
			{
				cv::Rect myROI(20, i - 11, (img1.cols - 30), 21);
				cv::Mat feedImage = img2(myROI);

				double minVal;
				Point Loc = visionMatching.TemplateMatching(img1Col, i, img1, feedImage, &minVal);

				double depth = coordinateConverter.ComputeZ(img1Col, Loc.x, baseline, fx);



				if (depth > 0)
				{
					if (maxVal < 1 / depth)
					{
						maxVal = 1 / depth;
					}


					for (int row = i - 11; row <= i + 10; row++)
					{
						for (int count = img1Col - 11; count <= img1Col + 10; count++)
						{
							finalImage.at<double>(row, count) = 1 / depth;//Exception
						}
					}


					//circle(finalImage, Point(img1Col, i), 2, Scalar(depth), -1);
				}
			}
		}


#ifdef STEREO_DEBUG_MODE
		ofstream outfileInit;

		outfileInit.open(filesSavePath + "ImageMatInitialBeforeAnyOp.txt");

		outfileInit << finalImage;
#endif // DEBUG


		Mat out = (((finalImage / maxVal)) * 255);

#ifdef STEREO_DEBUG_MODE
		ofstream outfile_;

		outfile_.open(filesSavePath + "ImageMatIntermediate.txt");

		outfile_ << out;
#endif // DEBUG

		out.convertTo(out, CV_16UC1);

#ifdef STEREO_DEBUG_MODE
		ofstream outfile;

		outfile.open(filesSavePath + "finalImageMat.txt");

		outfile << out;

#endif // DEBUG


		out.convertTo(out, CV_8UC1);


		Mat final;
		final = out + weightMat;

#ifdef STEREO_DEBUG_MODE
		ofstream outfile__;

		outfile__.open(filesSavePath + "finalImageMat1byte.txt");

		outfile__ << final;
#endif // DEBUG


		for (int i = 50; i < (img1.rows - 100); i++)
		{
			for (int img1Col = 20; img1Col < (img1.cols - 30); img1Col++)
			{

				uchar currentVal = final.at<uchar>(i, img1Col);
				if (currentVal > 75 && currentVal < 85)
				{
					finalImageColoured.at<Vec3b>(i, img1Col)[0] = 200;//Blue
					finalImageColoured.at<Vec3b>(i, img1Col)[1] = 0;
					finalImageColoured.at<Vec3b>(i, img1Col)[2] = 0;
				}
				else if (currentVal >= 85 && currentVal < 92)
				{
					finalImageColoured.at<Vec3b>(i, img1Col)[0] = 0;
					finalImageColoured.at<Vec3b>(i, img1Col)[1] = 200;//Green
					finalImageColoured.at<Vec3b>(i, img1Col)[2] = 0;
				}
				else if (currentVal > 92 && currentVal < 130)
				{
					finalImageColoured.at<Vec3b>(i, img1Col)[0] = 0;
					finalImageColoured.at<Vec3b>(i, img1Col)[1] = 0;
					finalImageColoured.at<Vec3b>(i, img1Col)[2] = 200; //Red
				}
			}
		}

		cv::imwrite(filesSavePath + "finalImageOut_ColouredDepth.png",
			finalImageColoured);//finalImage Coloured

		cv::imwrite(filesSavePath + "finalImageOut_GreyScale.png",
			final);//finalImage


		return 1;
	}
	catch (Exception)
	{
		return -1;
	}
	
}


VISIONLIB_API int StereoTestUncalibratedCV(const char* imgLink1, const char* imgLink2)
{
	Mat imgL = imread(imgLink1, 0);
	Mat imgR = imread(imgLink2, 0);

	//Compute Disparity image using opencv disparity to compare with current created disparity generation process
	int minDisparity = 0;
	int numDisparities = 16;
	int blockSize = 5;
	cv::Ptr<cv::StereoBM> stereo = cv::StereoBM::create();
	stereo->setNumDisparities(16);
	stereo->setBlockSize(blockSize);
	stereo->setMinDisparity(minDisparity);

	Mat disp;
	Mat disparity;

	stereo->compute(imgL, imgR, disp);

	// Save the disparity map
	cv::imwrite(
		"C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\disparity.png",
		disp);

	disp.convertTo(disparity, CV_32F, 1.0);

	// Scaling down the disparity values and normalizing them 
	disparity = (disparity / 16.0f - (float)minDisparity) / ((float)numDisparities);


	//

	return 1;

}


//*Main Function for uncalibrated stereo*/
VISIONLIB_API int StereoTestUncalibrated(const char* imgLink1, const char* imgLink2)
{
	Mat imgL = imread(imgLink1, 0);
	Mat imgR = imread(imgLink2, 0);

	//Note: Add normalization for blocks of pixels in the final output mat

	Mat finalImage(imgL.rows, imgL.cols, CV_64FC1);//Use to store final image
	Mat finalImageColoured(imgL.rows, imgL.cols, CV_8UC3);


	/*First phase match keypoints in the two images*/
	VisionMatching visionMatching;
	CoordinateConverter coordinateConverter;
	
	//SiftFeatureDetector siftFeatureDetector;

	//siftFeatureDetector.create();

	Ptr<SIFT> siftDetector = SIFT::create();

	Mat descriptorImg1, descriptorImg2;
	vector<KeyPoint> keypointsImg1, keypointsImg2;

	siftDetector->detectAndCompute(imgL, noArray(),
		keypointsImg1, descriptorImg1);

	siftDetector->detectAndCompute(imgR, noArray(),
		keypointsImg2, descriptorImg2);

	//siftFeatureDetector.detectAndCompute(img1, noArray(), 
		//keypointsImg1, descriptorImg1);

	//siftFeatureDetector.detectAndCompute(img2, Mat(),
		//keypointsImg2, descriptorImg2);

	BFMatcher bfMatcher = BFMatcher(NORM_L2);
	//Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::BRUTEFORCE);
	std::vector< std::vector<DMatch> > matches;

	std::vector<DMatch>  bf_match_results;

	bfMatcher.match(descriptorImg1, descriptorImg2, bf_match_results, noArray());

	//matcher->Match(descriptorImg1, descriptorImg2, knn_matches, 2);

	vector< std::vector<DMatch> >::iterator ptr;

	

	int pointNumber = 0;
	int goodMatch = 0;

	/*for (size_t i = 0; i < 9; i++)
	{
		if (goodMatch >= 9)
			break;

		int count = (int)i;
		int index1 = knn_matches[count][0].queryIdx;
		int index2 = knn_matches[count][0].trainIdx;

		if (min(knn_matches[i][0].distance, knn_matches[i][1].distance)/
			max(knn_matches[i][0].distance, knn_matches[i][1].distance)> 0.8)
		{
			LeftRightCoord.at<float>(goodMatch, 0) = (float)keypointsImg1[index1].pt.x;
			LeftRightCoord.at<float>(goodMatch, 1) = (float)keypointsImg1[index1].pt.y;
			LeftRightCoord.at<float>(goodMatch, 2) = (float)keypointsImg2[index2].pt.x;
			LeftRightCoord.at<float>(goodMatch, 3) = (float)keypointsImg2[index2].pt.y;

			goodMatch++;
		}

		
	}*/

	Mat LeftRightCoord = Mat::zeros(9, 4, CV_32FC1);//Matching points use for fund matrix generation

	vector<Point2f> points1(9);
	vector<Point2f> points2(9);

	vector<DMatch> newResultsFiltered;

 	for (size_t i = 0; i < bf_match_results.size(); i++)
	{
		if (goodMatch >= 9)
			break;

		int count = (int)i;
		int index1 = bf_match_results[count].queryIdx;
		int index2 = bf_match_results[count].trainIdx;


		float imgLX = (float)keypointsImg1[index1].pt.x;
		float imgLY = (float)keypointsImg1[index1].pt.y;
		float imgRX = (float)keypointsImg2[index2].pt.x;
		float imgRY = (float)keypointsImg2[index2].pt.y;

		//Since current images used are only has X direction difference between cameras and 
		//located in the same y axis, good results can filter by selecting minimum y displaced images
		if ((abs(imgLY - imgRY)<1) && (abs(imgLY - imgRY) > 0))
		{
			int coordMatchIt = 0;

			while (coordMatchIt< goodMatch)
			{
				if ((imgLX== LeftRightCoord.at<float>(coordMatchIt, 0) &&
					imgLY== LeftRightCoord.at<float>(coordMatchIt, 1)) ||
					(imgRX == LeftRightCoord.at<float>(coordMatchIt, 2) &&
					imgRY == LeftRightCoord.at<float>(coordMatchIt, 3)))
				{
					break;
				}

				coordMatchIt++;
			}

			if (coordMatchIt == goodMatch)
			{
				points1[goodMatch].x = imgLX;
				points1[goodMatch].y = imgLY;
				points2[goodMatch].x = imgRX;
				points2[goodMatch].y = imgRY;

				LeftRightCoord.at<float>(goodMatch, 0) = imgLX;
				LeftRightCoord.at<float>(goodMatch, 1) = imgLY;
				LeftRightCoord.at<float>(goodMatch, 2) = imgRX;
				LeftRightCoord.at<float>(goodMatch, 3) = imgRY;

				goodMatch++;

				newResultsFiltered.push_back(bf_match_results[count]);
			}

		}

	}

	ofstream outfileInit;

	outfileInit.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\\Results\\\\UncalibrateStereoMatchedPoints.txt");

	outfileInit << LeftRightCoord;

	/*vector<DMatch>::const_iterator first = bf_match_results.begin() + 50;
	vector<DMatch>::const_iterator last = bf_match_results.begin() + 60;
	vector<DMatch> newResults(first, last);*/

	Mat finalImg;
	cv::drawMatches(imgL, keypointsImg1, imgR, keypointsImg2, newResultsFiltered, finalImg);

	cv::imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\UncalibratedStereo\\FinalMatchedResult.png",
		finalImg);

	Mat FundamentalMat;
	Mat EssentialMat;

	UncalibratedStereo uncalibratedStereo;

	//Fundamental Matrix estimation is sensitive to quality of matches, outliers etc.
	//It becomes worse when all selected matches lie on the same plane. - This Stated Opencv
	FundamentalMat = uncalibratedStereo.GenerateFundMat(LeftRightCoord);
	
	//
	Mat cv_FundOut  = findFundamentalMat(points1, points2, FM_LMEDS);

	cv_FundOut.convertTo(cv_FundOut, CV_32FC1);

	ofstream outfileCVFund;

	outfileCVFund.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\\Results\\FundamentalMatrixCV.txt");

	outfileCVFund << cv_FundOut;

	//

	EssentialMat = uncalibratedStereo.GenerateEssentialMat(cv_FundOut);//FundamentalMat

	uncalibratedStereo.FindTAndR(EssentialMat);

	Mat R, Tx;

	uncalibratedStereo.ComputePl();
	uncalibratedStereo.ComputeMr();

	double maxDepth = DBL_MIN;
	double minDepth = DBL_MAX;

	for (int i = 50; i < imgL.rows-20; i += 21)
	{
		for (int img1Col = 20; img1Col < imgL.cols-20; img1Col += 21)
		{
			cv::Rect myROI(20, i - 11, (imgL.cols-20), 21);
			cv::Mat feedImage = imgR(myROI);

			double minVal;
			Point Loc = visionMatching.TemplateMatching(img1Col, i, imgL, feedImage, &minVal);

			Point L;
			Point R;

			L.x = img1Col;
			L.y = i;

			R.x = Loc.x;
			R.y = Loc.y;

			double zr;

			zr = abs(uncalibratedStereo.ComputeDepth(L, R));

			if (maxDepth < zr)
				maxDepth = zr;


			if (zr > 0)
			{
				if (minDepth > zr)
					minDepth = zr;

				for (int row = i - 11; row <= i + 10; row++)
				{
					for (int count = img1Col - 11; count <= img1Col + 10; count++)
					{
						

						finalImage.at<double>(row, count) = zr;
					}
				}


				//circle(finalImage, Point(img1Col, i), 2, Scalar(depth), -1);
			}
		}

	}

	cv::imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\UnCalibratedMatImg.png",
		finalImage);//finalImage without converting to 0-255

	ofstream outfile;

	outfile.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\\Results\\UnCalibratedFinalMat.txt");

	outfile << finalImage;

	Mat out = (((finalImage / maxDepth)) * 255);

	out.convertTo(out, CV_8UC1);

	ofstream outfile_;

	outfile_.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\\Results\\UnCalibratedFinalMatConverted.txt");

	outfile_ << out;

	/*UINT8 minGrayValue = (UINT8)((minDepth / maxDepth) * 255);
	UINT8 interval = (UINT8)((255 - minGrayValue) / 10);

	for (int i = 50; i < imgL.rows; i++)
	{
		for (int img1Col = 20; img1Col < imgL.cols; img1Col++)
		{

			uchar currentVal = out.at<uchar>(i, img1Col);
			
			int iteration = 0;
			while (currentVal > (minGrayValue+(iteration*interval)))
			{
				finalImageColoured.at<Vec3b>(i, img1Col)[0] = (iteration * interval);//Blue
				finalImageColoured.at<Vec3b>(i, img1Col)[1] = 10;//Green
				finalImageColoured.at<Vec3b>(i, img1Col)[2] = 10;//Red

				iteration++;
			}
			
		}
	}*/

	cv::imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\UnCalibratedFinalMatImg.png",
		out);//finalImage

	cv::imwrite("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\UnCalibratedFinalMatImgColored.png",
		finalImageColoured);//finalImage

	return 1;
}