#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>


class VisionMatching
{
public:
	cv::Point TemplateMatching(int pointX, int pointY, cv::Mat img1, cv::Mat img2, double* minimumVal);
	void PointMatching(int img1X, int img1Y, int img2X, int img2Y,
		cv::Mat img1, cv::Mat img2, double* minimumVal);
	
};

