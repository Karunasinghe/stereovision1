#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>


class UncalibratedStereo
{
	

public:
	cv::Mat R;
	cv::Mat Tx;
	cv::Mat Pl;
	cv::Mat Mr = cv::Mat::zeros(3, 4, CV_32F);
	cv::Mat GenerateFundMat(cv::Mat LeftRightCoord);
	cv::Mat GenerateEssentialMat(cv::Mat Fundamental);
	void FindTAndR(cv::Mat EssentialMat);
	void ComputePl();
	void ComputeMr();
	double ComputeDepth(cv::Point L, cv::Point R);
	


};

