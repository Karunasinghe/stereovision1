#include "pch.h"
#include "UncalibratedStereo.h"

#include <fstream>
#include <iostream>

using namespace cv;
using namespace std;

/*Function that generate Fundamental matrix vaules using SVD and Coordinates of left and right matched point pairs*/
cv::Mat UncalibratedStereo:: GenerateFundMat(Mat LeftRightCoord)
{
	Mat A = Mat::zeros(9, 9, CV_32FC1);
	for (int i=0; i<9; i++)
	{
		A.at<float>(i, 0) = (float)(LeftRightCoord.at<float>(i, 0)* LeftRightCoord.at<float>(i, 2));
		A.at<float>(i, 1) = (float)(LeftRightCoord.at<float>(i, 0) * LeftRightCoord.at<float>(i, 3));
		A.at<float>(i, 2) = (float)LeftRightCoord.at<float>(i, 0);

		A.at<float>(i, 3) = (float)(LeftRightCoord.at<float>(i, 2) * LeftRightCoord.at<float>(i, 1));
		A.at<float>(i, 4) = (float)(LeftRightCoord.at<float>(i, 1) * LeftRightCoord.at<float>(i, 3));
		A.at<float>(i, 5) = (float)LeftRightCoord.at<float>(i, 1);

		A.at<float>(i, 6) = (float)LeftRightCoord.at<float>(i, 2);
		A.at<float>(i, 7) = (float)LeftRightCoord.at<float>(i, 3);
		A.at<float>(i, 8) = 1;
	}

	/*Logging Left Right Coordinates combination matrix*/
	ofstream outfileLeftRightComb;

	outfileLeftRightComb.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\LeftRightAMat.txt");

	outfileLeftRightComb << A;

	Mat U, W, VT;

	SVD::compute(A, W, U, VT);

	Mat FundamentalMat = Mat::zeros(3, 3, CV_32F);

	int rowNumber = 0;
	for (int i = 0; i < 9; i++)
	{
		if (i % 3 == 0 && i!=0)
			rowNumber++;

		FundamentalMat.at<float>(rowNumber, i%3) = VT.at<float>(i, 8);
	}
		
	/*Logging Fundamental Matrix*/
	ofstream outfile;

	outfile.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\FundamentalMatrix.txt");

	outfile << FundamentalMat;

	return FundamentalMat;
}

cv::Mat UncalibratedStereo:: GenerateEssentialMat(Mat Fundamental)
{
	Mat EssentialMat;

	try 
	{
		Mat KlT = Mat::zeros(3, 3, CV_32F);

		KlT.at<float>(0, 0) = (float)4412.147;//fx left cam  (Ambient 1st DataSet 3979.911)
		KlT.at<float>(2, 0) = (float)1283.781;//Ox left cam  1244.772
		KlT.at<float>(1, 1) = (float)4412.147;//fy left cam   3979.911
		KlT.at<float>(2, 1) = (float)994.694;//Oy left cam   1019.507
		KlT.at<float>(2, 2) = 1;

		Mat Kr = Mat::zeros(3, 3, CV_32F);
		Kr.at<float>(0, 0) = (float)4412.147;//fx right cam   3979.911
		Kr.at<float>(0, 2) = (float)1406.974;//Ox right cam   1369.115
		Kr.at<float>(1, 1) = (float)4412.147;//fy right cam   3979.911
		Kr.at<float>(1, 2) = (float)994.694;//Oy right cam   1019.507
		Kr.at<float>(2, 2) = 1;

		EssentialMat = KlT * Fundamental * Kr;

		/*Logging Essential Matrix*/
		ofstream outfile;
		outfile.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\EssentialMatrix.txt");

		outfile << EssentialMat;
	}
	catch (Exception)
	{

	}
	

	return EssentialMat;
}

void UncalibratedStereo::FindTAndR(Mat EssentialMat)
{
	//E = Tx * R

	//Tx is skew Symmetric, Therefore Transpose(Tx) = -Tx
	//R is Orthonormal, R * Transpose(R) =I
    //E = Tx * R
	//Tx * R = U * W * VT
	//Since R is orthonormal, R = VT
	//Tx = U * W
	Mat U, W, VT;
	SVD::compute(EssentialMat, W, U, VT);


	//Tx = U*W*w*Ut  (Ut= U transpose)
	//w = [0 -1 0; 1 0 0; 0 0 1]
	//wt = [0 1 0; -1 0 0; 0 0 1] w transpose

	//Refer: https://en.wikipedia.org/wiki/Essential_matrix

	Mat w = Mat::zeros(3, 3, CV_32F);
	w.at<float>(0, 1) = -1;
	w.at<float>(1, 0) = 1;
	w.at<float>(2, 2) = 1;


	Mat w_inv = Mat::zeros(3, 3, CV_32F);
	w_inv.at<float>(0, 1) = 1;
	w_inv.at<float>(1, 0) = -1;
	w_inv.at<float>(2, 2) = 1;

	Mat Ut = Mat::zeros(3, 3, CV_32F);
	transpose(U, Ut);

	Mat Wdiag = Mat::zeros(3, 3, CV_32F);
	Wdiag.at<float>(0, 1) = W.at<float>(0, 0);
	Wdiag.at<float>(1, 1) = W.at<float>(1, 0);
	Wdiag.at<float>(2, 2) = W.at<float>(2, 0);

	Tx = U* w* Wdiag* Ut;

	R = U* w_inv* VT;

}

void UncalibratedStereo::ComputePl()
{
	Mat Ml = Mat::zeros(3, 4, CV_32F);
	Mat R_and_T = Mat::zeros(4, 4, CV_32F);


	Ml.at<float>(0, 0) = (float)1758.23;
	Ml.at<float>(0, 2) = (float)953.34;
	Ml.at<float>(1, 1) = (float)1758.23;
	Ml.at<float>(1, 2) = (float)552.29;
	Ml.at<float>(2, 2) = 1;

	R_and_T.at<float>(0, 0) = 0;// R.at<float>(0, 0);
	R_and_T.at<float>(0, 1) = 0;// R.at<float>(0, 1);
	R_and_T.at<float>(0, 2) = 0;// R.at<float>(0, 2);
	R_and_T.at<float>(0, 3) = (float)111.53;//Tx.at<float>(2, 1); //tx position
	
	R_and_T.at<float>(1, 0) = 0;//R.at<float>(1, 0);
	R_and_T.at<float>(1, 1) = 0;//R.at<float>(1, 1);
	R_and_T.at<float>(1, 2) = 0;// R.at<float>(1, 2);
	R_and_T.at<float>(1, 3) = 0;//Tx.at<float>(0, 2); //ty position

	R_and_T.at<float>(2, 0) = 0;//R.at<float>(2, 0);
	R_and_T.at<float>(2, 1) = 0;// R.at<float>(2, 1);
	R_and_T.at<float>(2, 2) = 0;// R.at<float>(2, 2);
	R_and_T.at<float>(2, 3) = 0;// Tx.at<float>(1, 0); //tz position

	R_and_T.at<float>(3, 3) = 1; 

	Pl = Ml * R_and_T;

	ofstream outfile_;

	outfile_.open("C:\\Users\\Asus\\Documents\\Robotics\\Computer vision\\Stereo\\ambient\\Results\\RAndTUncalib.txt");

	outfile_ << R_and_T;

}

void UncalibratedStereo::ComputeMr()
{
	//Mr = Mat::zeros(3, 4, CV_32F);

	Mr.at<float>(0, 0) = (float)1758.23;
	Mr.at<float>(0, 2) = (float)953.34;
	Mr.at<float>(1, 1) = (float)1758.23;
	Mr.at<float>(1, 2) = (float)552.29;
	Mr.at<float>(2, 2) = 1;

}

double UncalibratedStereo::ComputeDepth(cv::Point L, cv::Point R)
{
	
	//MatLeft*Ur = MatRight (Ur = [xr yr zr])
	Mat MatLeft = Mat::zeros(4, 3, CV_32F);//original size 4x3

	MatLeft.at<float>(0, 0) = (R.x*Mr.at<float>(2, 0))- Mr.at<float>(0, 0);
	MatLeft.at<float>(0, 1) = (R.x * Mr.at<float>(2, 1)) - Mr.at<float>(0, 1);
	MatLeft.at<float>(0, 2) = (R.x * Mr.at<float>(2, 2)) - Mr.at<float>(0, 2);

	MatLeft.at<float>(1, 0) = (R.y * Mr.at<float>(2, 0)) - Mr.at<float>(1, 0);
	MatLeft.at<float>(1, 1) = (R.y * Mr.at<float>(2, 1)) - Mr.at<float>(1, 1);
	MatLeft.at<float>(1, 2) = (R.y * Mr.at<float>(2, 2)) - Mr.at<float>(1, 2);

	MatLeft.at<float>(2, 0) = (L.x * Pl.at<float>(2, 0)) - Pl.at<float>(0, 0);
	MatLeft.at<float>(2, 1) = (L.x * Pl.at<float>(2, 1)) - Pl.at<float>(0, 1);
	MatLeft.at<float>(2, 2) = (L.x * Pl.at<float>(2, 2)) - Pl.at<float>(0, 2);

	MatLeft.at<float>(3, 0) = (L.y * Pl.at<float>(2, 0)) - Pl.at<float>(1, 0);
	MatLeft.at<float>(3, 1) = (L.y * Pl.at<float>(2, 1)) - Pl.at<float>(1, 1);
	MatLeft.at<float>(3, 2) = (L.y * Pl.at<float>(2, 2)) - Pl.at<float>(1, 2);

	Mat MatRight = Mat::zeros(4, 1, CV_32F);//original size 4x1

	MatRight.at<float>(0, 0) = Mr.at<float>(0, 3) - Mr.at<float>(2, 3);
	MatRight.at<float>(1, 0) = Mr.at<float>(1, 3) - Mr.at<float>(2, 3);
	MatRight.at<float>(2, 0) = Pl.at<float>(0, 3) - Pl.at<float>(2, 3);
	MatRight.at<float>(3, 0) = Pl.at<float>(1, 3) - Pl.at<float>(2, 3);

	Mat result;
	//Mat MatLeftInv;

	//invert(MatLeft, MatLeftInv);

	solve(MatLeft, MatRight, result, DECOMP_SVD);
	//result = MatLeftInv * MatRight;

	double zr = result.at<float>(2, 0);

	return zr;
}