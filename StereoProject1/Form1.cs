﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StereoProject1
{
    public partial class Form1 : Form
    {
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
         

        public Form1()
        {
            InitializeComponent();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1.DoWork += 
                new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.ProgressChanged +=
                new ProgressChangedEventHandler(
            this.backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted +=
                new RunWorkerCompletedEventHandler(
            backgroundWorker1_RunWorkerCompleted);
        }


        // This event handler is where the actual,
        // potentially time-consuming work is done.
        private void backgroundWorker1_DoWork(object sender,
            DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            worker.ReportProgress(1);

            e.Result = ComputeCalibratedStereo();

            
        }

        private void backgroundWorker1_ProgressChanged(object sender,
           ProgressChangedEventArgs e)
        {
            lbl_test.Text = "Callibrated Stereo Processing...";
        }

        private void backgroundWorker1_RunWorkerCompleted(
           object sender, RunWorkerCompletedEventArgs e)
        {
            lbl_test.Text = "Callibrated Stereo Finished Processing!!!";
        }


        StringBuilder img1StrBuilder = new StringBuilder();
        StringBuilder img2StrBuilder = new StringBuilder();

        StringBuilder baselineStrBuilder = new StringBuilder();
        StringBuilder fxStrBuilder = new StringBuilder();

        private void btn_test_Click(object sender, EventArgs e)
        {
            if (img1StrBuilder.Length == 0 || img2StrBuilder.Length==0)
            {
                MessageBox.Show("Please Load Both Left and Right Images");
                return;
            }

            /*string img1 = img1StrBuilder.ToString();// @"C:\Users\Asus\Documents\Robotics\Computer vision\Stereo\ambient\im0Left.png";
            string img2 = img2StrBuilder.ToString();// @"C:\Users\Asus\Documents\Robotics\Computer vision\Stereo\ambient\im1Right.png";

            lbl_test.Text = ComputeCalibratedStereo();*/
            if (backgroundWorker1.IsBusy != true)
            {
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void btn_uncalibrated_stereo_Click(object sender, EventArgs e)
        {
            string img1 = @"C:\Users\Asus\Documents\Robotics\Computer vision\Stereo\ImageSet2\im3Left.png";
            string img2 = @"C:\Users\Asus\Documents\Robotics\Computer vision\Stereo\ImageSet2\im3Right.png";

            bool useNewStereoVision = true;
            
            if(useNewStereoVision)
                lbl_test.Text = Vision.StereoTestUncalibrated(img1, img2).ToString();
            else
                lbl_test.Text = Vision.StereoTestUncalibratedCV(img1, img2).ToString();


        }

        private void btn_load_img1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";

            if (open.ShowDialog() == DialogResult.OK)
            {
                pcb_img1.SizeMode = PictureBoxSizeMode.StretchImage;
                // display image in picture box  
                pcb_img1.Image = new Bitmap(open.FileName);

                string fileLocation = open.FileName;

                if (img1StrBuilder.Length!=0)
                {
                    img1StrBuilder.Clear();
                }

                img1StrBuilder = new StringBuilder(fileLocation);

            }
        }

        private void btn_load_img2_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";

            if (open.ShowDialog() == DialogResult.OK)
            {
                pcb_img2.SizeMode = PictureBoxSizeMode.StretchImage;
                // display image in picture box  
                pcb_img2.Image = new Bitmap(open.FileName);

                string fileLocation = open.FileName;

                if (img2StrBuilder.Length != 0)
                {
                    img2StrBuilder.Clear();
                }

                img2StrBuilder = new StringBuilder(fileLocation);

            }
        }

        private string ComputeCalibratedStereo()
        {
            try
            {
                string img1 = img1StrBuilder.ToString();// @"C:\Users\Asus\Documents\Robotics\Computer vision\Stereo\ambient\im0Left.png";
                string img2 = img2StrBuilder.ToString();

                if (baselineStrBuilder.Length == 0 || fxStrBuilder.Length == 0)
                {
                    MessageBox.Show("Please Enter Baseline and Fx values");
                    return "Process Cancelled";
                }

                double baseline = double.Parse(baselineStrBuilder.ToString(),
                    System.Globalization.CultureInfo.InvariantCulture);
                double fx = double.Parse(fxStrBuilder.ToString(),
                    System.Globalization.CultureInfo.InvariantCulture); ;

                return Vision.StereoTest(img1, img2, baseline, fx).ToString();
            }
            catch (Exception)
            {
                return "Exception Occured";            
            }
            
        }

        private void txt_baseline_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (baselineStrBuilder.Length != 0)
                    baselineStrBuilder.Clear();

                baselineStrBuilder = new StringBuilder(txt_baseline.Text);
            }
            catch (Exception)
            { 
            }
        }

        private void txt_fx_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (fxStrBuilder.Length != 0)
                    fxStrBuilder.Clear();

                fxStrBuilder = new StringBuilder(txt_fx.Text);
            }
            catch (Exception)
            {
            }
        }
    }
}
