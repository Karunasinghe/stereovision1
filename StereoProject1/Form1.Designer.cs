﻿
namespace StereoProject1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_test = new System.Windows.Forms.Button();
            this.lbl_test = new System.Windows.Forms.Label();
            this.btn_uncalibrated_stereo = new System.Windows.Forms.Button();
            this.pcb_img1 = new System.Windows.Forms.PictureBox();
            this.pcb_img2 = new System.Windows.Forms.PictureBox();
            this.btn_load_img1 = new System.Windows.Forms.Button();
            this.btn_load_img2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_baseline = new System.Windows.Forms.TextBox();
            this.txt_fx = new System.Windows.Forms.TextBox();
            this.lbl_baseline = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_img1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_img2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_test
            // 
            this.btn_test.Location = new System.Drawing.Point(27, 359);
            this.btn_test.Name = "btn_test";
            this.btn_test.Size = new System.Drawing.Size(109, 58);
            this.btn_test.TabIndex = 0;
            this.btn_test.Text = "Execute";
            this.btn_test.UseVisualStyleBackColor = true;
            this.btn_test.Click += new System.EventHandler(this.btn_test_Click);
            // 
            // lbl_test
            // 
            this.lbl_test.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_test.Location = new System.Drawing.Point(128, 297);
            this.lbl_test.Name = "lbl_test";
            this.lbl_test.Size = new System.Drawing.Size(739, 43);
            this.lbl_test.TabIndex = 1;
            this.lbl_test.Text = "_ _ _ _";
            // 
            // btn_uncalibrated_stereo
            // 
            this.btn_uncalibrated_stereo.Enabled = false;
            this.btn_uncalibrated_stereo.Location = new System.Drawing.Point(364, 359);
            this.btn_uncalibrated_stereo.Name = "btn_uncalibrated_stereo";
            this.btn_uncalibrated_stereo.Size = new System.Drawing.Size(109, 58);
            this.btn_uncalibrated_stereo.TabIndex = 2;
            this.btn_uncalibrated_stereo.Text = "Uncalibrated Stereo";
            this.btn_uncalibrated_stereo.UseVisualStyleBackColor = true;
            this.btn_uncalibrated_stereo.Click += new System.EventHandler(this.btn_uncalibrated_stereo_Click);
            // 
            // pcb_img1
            // 
            this.pcb_img1.Location = new System.Drawing.Point(27, 12);
            this.pcb_img1.Name = "pcb_img1";
            this.pcb_img1.Size = new System.Drawing.Size(252, 166);
            this.pcb_img1.TabIndex = 3;
            this.pcb_img1.TabStop = false;
            // 
            // pcb_img2
            // 
            this.pcb_img2.Location = new System.Drawing.Point(364, 13);
            this.pcb_img2.Name = "pcb_img2";
            this.pcb_img2.Size = new System.Drawing.Size(264, 165);
            this.pcb_img2.TabIndex = 4;
            this.pcb_img2.TabStop = false;
            // 
            // btn_load_img1
            // 
            this.btn_load_img1.Location = new System.Drawing.Point(27, 225);
            this.btn_load_img1.Name = "btn_load_img1";
            this.btn_load_img1.Size = new System.Drawing.Size(109, 50);
            this.btn_load_img1.TabIndex = 5;
            this.btn_load_img1.Text = "Load Left Image";
            this.btn_load_img1.UseVisualStyleBackColor = true;
            this.btn_load_img1.Click += new System.EventHandler(this.btn_load_img1_Click);
            // 
            // btn_load_img2
            // 
            this.btn_load_img2.Location = new System.Drawing.Point(364, 225);
            this.btn_load_img2.Name = "btn_load_img2";
            this.btn_load_img2.Size = new System.Drawing.Size(109, 50);
            this.btn_load_img2.TabIndex = 6;
            this.btn_load_img2.Text = "Load Right Image";
            this.btn_load_img2.UseVisualStyleBackColor = true;
            this.btn_load_img2.Click += new System.EventHandler(this.btn_load_img2_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 297);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 43);
            this.label1.TabIndex = 7;
            this.label1.Text = "Status";
            // 
            // txt_baseline
            // 
            this.txt_baseline.Location = new System.Drawing.Point(749, 225);
            this.txt_baseline.Name = "txt_baseline";
            this.txt_baseline.Size = new System.Drawing.Size(134, 22);
            this.txt_baseline.TabIndex = 8;
            this.txt_baseline.TextChanged += new System.EventHandler(this.txt_baseline_TextChanged);
            // 
            // txt_fx
            // 
            this.txt_fx.Location = new System.Drawing.Point(749, 272);
            this.txt_fx.Name = "txt_fx";
            this.txt_fx.Size = new System.Drawing.Size(134, 22);
            this.txt_fx.TabIndex = 9;
            this.txt_fx.TextChanged += new System.EventHandler(this.txt_fx_TextChanged);
            // 
            // lbl_baseline
            // 
            this.lbl_baseline.AutoSize = true;
            this.lbl_baseline.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_baseline.Location = new System.Drawing.Point(644, 225);
            this.lbl_baseline.Name = "lbl_baseline";
            this.lbl_baseline.Size = new System.Drawing.Size(82, 20);
            this.lbl_baseline.TabIndex = 10;
            this.lbl_baseline.Text = "Baseline";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(648, 277);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fx";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 473);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_baseline);
            this.Controls.Add(this.txt_fx);
            this.Controls.Add(this.txt_baseline);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_load_img2);
            this.Controls.Add(this.btn_load_img1);
            this.Controls.Add(this.pcb_img2);
            this.Controls.Add(this.pcb_img1);
            this.Controls.Add(this.btn_uncalibrated_stereo);
            this.Controls.Add(this.lbl_test);
            this.Controls.Add(this.btn_test);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pcb_img1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_img2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_test;
        private System.Windows.Forms.Label lbl_test;
        private System.Windows.Forms.Button btn_uncalibrated_stereo;
        private System.Windows.Forms.PictureBox pcb_img1;
        private System.Windows.Forms.PictureBox pcb_img2;
        private System.Windows.Forms.Button btn_load_img1;
        private System.Windows.Forms.Button btn_load_img2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_baseline;
        private System.Windows.Forms.TextBox txt_fx;
        private System.Windows.Forms.Label lbl_baseline;
        private System.Windows.Forms.Label label2;
    }
}

