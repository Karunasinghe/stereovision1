﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;//DLL support library

namespace StereoProject1
{
    class Vision
    {
        [DllImport(@"VisionLib.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int StereoTest(string img1, string img2, double baseline, double fx);

        [DllImport(@"VisionLib.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int StereoTestUncalibrated(string img1, string img2);

        [DllImport(@"VisionLib.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int StereoTestUncalibratedCV(string img1, string img2);
    }
}
